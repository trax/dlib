#include <string>
#include <fstream>
#include <random>
#include <dlib/dnn.h>
#include <iostream>
#include <dlib/data_io.h>

using namespace dlib;

using Input = matrix<unsigned char, 0, 1>;
using Inputs = std::vector<Input>;
using Label = long unsigned int;
using Labels = std::vector<Label>;

std::random_device rd;
const static auto output_size = 255u;
const static auto nb_char = 8u;
const static auto input_size = nb_char*output_size;
const static auto mini_batch_size = 128u;


void make_mini_batch(const std::string &str, Inputs &inputs, Labels &labels) {
  static std::uniform_int_distribution<int> dist(0, str.size() - (nb_char + 2));
  // std::cout << "pos> " << s << std::endl;
  for (auto n = 0u ; n < mini_batch_size ; n++) {
    const auto s = dist(rd);
    for (auto i = 0u ; i < nb_char ; i++) {
      const auto c = str[s + i];
      // std::cout << c;
      for (auto j = 0u ; j < output_size ; j++) {
	inputs[n](0, j+i*output_size) = (j == c)?1:0;
	// std::cout << ((j == c)?1:0);
      }
      // std::cout << std::endl;
    }
    labels[n] = str[s + nb_char];
    // std::cout << ">" << str[s + nb_char] << std::endl;
  }
}
using net_type = loss_multiclass_log<
  fc<output_size, fc<2048, relu<fc<input_size, input<Input>>>>>>;

void train(const std::string &input_path,
	   const std::string &output_path) {
  std::ifstream t(input_path);
  const std::string str((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());


  
  net_type net;

  std::cout << net << std::endl;
  
  dnn_trainer<net_type,adam> trainer(net,adam(0.0005, 0.9, 0.999));
  trainer.be_verbose();
  trainer.set_iterations_without_progress_threshold(2000);
  trainer.set_learning_rate_shrink_factor(0.1);
  trainer.set_learning_rate(1e-3);
  trainer.set_synchronization_file({output_path + ".sync"}, std::chrono::seconds(100));
      
  Inputs inputs(mini_batch_size);
  Labels labels(mini_batch_size);
  for (auto i = 0u ; i < mini_batch_size ; i++) {
    inputs[i].set_size(input_size);
  }
  

  while(trainer.get_learning_rate() >= 1e-6) {
    make_mini_batch(str, inputs, labels);
    // for (const auto input : inputs[1]) {
    //   std::cout << input; 
    // }std::cout << ":" << (char)labels[1] << std::endl;
    trainer.train_one_step(inputs, labels);
  }

  // std::cout << layer<0>(net).get_layer_params() << std::endl;
  std::cout << net << std::endl;
  
  // auto la = layer<2>(net);
  // auto output = la.get_weights();
  // for (const auto e : output) {
  //   std::cout << e << std::endl;
  // }

  serialize(output_path) << net;
  make_mini_batch(str, inputs, labels);
  std::vector<unsigned long int> predicted_labels = net(inputs);
  int diff = 0;
  for (auto i = 0u ; i <  predicted_labels.size() ; i++) {
    std::cout << predicted_labels[i] << " " << labels[i] << std::endl;
    diff += ((predicted_labels[i] == labels[i])?0:1);
  }

  std::cout << diff << "/" << predicted_labels.size() << std::endl;

}

void run(const std::string &net_path) {
  net_type net;
  deserialize(net_path) >> net;
  const auto a = std::string{"Anarchists including the [[The Anarchy Organisation]] and [[Murray Rothbard|Rothbard]] find"};
  Inputs inputs(1);
  inputs[0].set_size(input_size);

  for (int i = 0 ; i < input_size ; i++) {
    inputs[0](0, i) = a[i];
    std::cout << a[i];
  }std::cout << ":";

  for (int i = 0 ; i < 100 ; i++) {
    const auto c = net(inputs);
    std::cout << c[0] << std::endl;;
    for (int i = 1 ; i < input_size ; i++) {
      inputs[0](0, i-1) = inputs[0](0, i);
      std::cout << (char)inputs[0](0, i-1);
    }
    inputs[0](0, input_size - 1) = c[0];
  }
}

int main(int argc, char *argv[]) {
  if(argc == 1) {
    return 1;
  }

  std::cout << argv[1] << std::endl;
  
  if(std::string{argv[1]} == "train") {
    if(argc != 4) {
      std::cerr << "wrong args" << std::endl;
      return 1;
    }
    const auto input_path = argv[2];
    const auto output_path = argv[3];
    train(input_path, output_path);
    return 0;
  }

  if(std::string{argv[1]} == "run") {
    if(argc != 3) {
      std::cerr << "wrong args" << std::endl;
	return 1;
    }

    const auto net_path = argv[2];
    run(net_path);
    return 0;
  }


  if(std::string{argv[1]} == "print") {
    if(argc != 3) {
      std::cerr << "wrong args" << std::endl;
      return 1;
    }

    const auto net_path = argv[2];
    net_type net;
    deserialize(net_path) >> net;
    std::cout << net << std::endl;
    auto la = layer<1>(net);
    auto it = la.get_output().begin();
    while(it != la.get_output().end()) {
      std::cout <<*it<< std::endl;
      ++it;
    }
    return 0;
  }

  
  std::cout << "wrong command or args" << std::endl;
  return 1;
}
